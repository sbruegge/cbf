import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32MultiArray, MultiArrayDimension, MultiArrayLayout
from ackermann_msgs.msg import AckermannDriveStamped
import time
import os
import numpy as np
import sympy as sp
import time
from .cbf_submodule.car_model import Car
from joblib import load

NODE_NAME = 'pid_node'
POSE_TOPIC_NAME = '/slam_out_pose'
ERROR_TOPIC_NAME = '/error'
ACTUATOR_TOPIC_NAME = '/teleop'
JOY_TOPIC_NAME = '/joy/teleop'


class CbfSafetyFilter(Node):
    def __init__(self):
        super().__init__(NODE_NAME)
        self.QUEUE_SIZE = 10
        
        # Setup multi-threads
        self.pose_thread = MutuallyExclusiveCallbackGroup()
        self.imu_thread = MutuallyExclusiveCallbackGroup()
        self.odom_thread = MutuallyExclusiveCallbackGroup()
        self.joy_thread = MutuallyExclusiveCallbackGroup()

        # Actuator control
        self.drive_pub = self.create_publisher(AckermannDriveStamped, ACTUATOR_TOPIC_NAME, self.QUEUE_SIZE)
        self.drive_cmd = AckermannDriveStamped()

        ### Get sensor measurements ###
        # Get GPS/Lidar measurements
        self.pose_subscriber = self.create_subscription(PoseStamped, POSE_TOPIC_NAME, self.pose_measurement, self.QUEUE_SIZE, callback_group=self.pose_thread)
        self.pose_subscriber

        # Get IMU measurement
        self.imu_subscriber = self.create_subscription(Imu, IMU_TOPIC_NAME, self.imu_measurement, self.QUEUE_SIZE, callback_group=self.imu_thread)
        self.imu_subscriber

        # Get Odometry measurements
        self.odom_subscriber = self.create_subscription(Odometry, ODOM_TOPIC_NAME, self.odom_measurement, self.QUEUE_SIZE, callback_group=self.odom_thread)
        self.odom_subscriber

        # Get Joystick commands
        self.joy_subscriber = self.create_subscription(AckermannDriveStamped, JOY_TOPIC_NAME, self.set_joy_command, self.QUEUE_SIZE, callback_group=self.joy_thread)
        self.joy_subscriber

        # setting up message structure for vesc-ackermann msg
        self.current_time = self.get_clock().now().to_msg()
        self.frame_id = 'base_link'

        # Default actuator and CBF values (if not given in config file)
        self.declare_parameters(
            namespace='',
            parameters=[
                # Actuator parameters
                ('zero_speed', 0.0),
                ('max_speed', 5),
                ('min_speed', 0.1),
                ('desired_speed', 1.0),
                ('max_right_steering', 0.4),
                ('max_left_steering', -0.4),
                ('Ts', 0.05),
                #  CBF parameters
                ('alpha', 1.0),
                ('c1_gain', 0.7),
                ('c2_gain', 0.7),
                ('observe_computation_time', False),
                ('svm_model_name', "model.joblib")
            ])
        
        # Actuator and CBF values
        self.zero_speed = self.get_parameter('zero_speed').value  # should be around 0
        self.max_speed = self.get_parameter('max_speed').value  # between [0,5] m/s
        self.min_speed = self.get_parameter('min_speed').value  # between [0,5] m/s 
        self.desired_speed =  self.get_parameter('desired_speed').value  # constant
        self.max_right_steering = self.get_parameter('max_right_steering').value  # negative(max_left) 
        self.max_left_steering = self.get_parameter('max_left_steering').value  # between abs([0,0.436332]) radians (0-25degrees)
        self.alpha = self.get_parameter('alpha').value  # offset of EDF (\delta in paper)
        self.c1_gain = self.get_parameter('c1_gain').value  # CBF gain
        self.c2_gain = self.get_parameter('c2_gain').value  # CBF gain
        self.Ts = self.get_parameter('Ts').value
        self.observe_computation_time = self.get_parameter('observe_computation_time').value  # Log computing time 
        self.svm_model_name = self.get_parameter('svm_model_name').value  # Model parameter

        # Initilize class variables
        self.car = Car(self.desired_speed)  # Car class includes car parameters
        self.x = np.zeros(5)  # State vector
        self.svm_model = load(self.svm_model_name)  # trained SVM model
        self.svm_params = {"b": cbf_model.intercept_, "dc": cbf_model.dual_coef_, "sv": cbf_model.support_vectors_, "gamma": self.svm_model.get_params["gamma"]}  # SVM parameters

        self.get_logger().info(
            f'\n zero_speed: {self.zero_speed}'
            f'\n max_speed: {self.max_speed}'
            f'\n min_speed: {self.min_speed}'
            f'\n desired_speed: {self.desired_speed}'
            f'\n max_right_steering: {self.max_right_steering}'
            f'\n max_left_steering: {self.max_left_steering}'
            f'\n alpha: {self.alpha}'
            f'\n c1_gain: {self.c1_gain}'
            f'\n c2_gain: {self.c2_gain}'
            f'\n Ts: {self.Ts}'
            f'\n observe_computation_time: {self.observe_computation_time}'
            f'\n svm_model_name: {self.svm_model_name}'
        )
        
        # Call controller
        self.create_timer(self.Ts, self.cbf_controller)

        # end __init__

    def pose_measurement(self, pose_data):
        # car orientation
        quaternion = (pose_data.pose.orientation.x, pose_data.pose.orientation.y, pose_data.pose.orientation.z, pose_data.pose.orientation.w)
        euler = euler_from_quaternion(quaternion)
        self.yaw_pose_buffer = euler[2]

        # car coordinates
        self.x_pose_buffer = pose_data.pose.position.x
        self.y_pose_buffer = pose_data.pose.position.y
        # self.get_logger().info(f"Updating POSE (x): ({self.x})")

    def imu_measurement(self, imu_data):
        quaternion = (imu_data.orientation.x, imu_data.orientation.y, imu_data.orientation.z, imu_data.orientation.w)
        euler = euler_from_quaternion(quaternion)
        
        self.yaw_imu_buffer = euler[2]
        self.yaw_rate_imu_buffer = imu_data.angular_velocity.z
        # self.get_logger().info("Updating IMU (yaw,yaw_rate): {self.yaw_imu_buffer}, {self.yaw_imu_buffer}")

    def odom_measurement(self, odom_data):
        # car position
        self.x_vesc_buffer = odom_data.pose.pose.position.x
        self.y_vesc_buffer = odom_data.pose.pose.position.y

        # car linear velocity
        self.vx_vesc_buffer = odom_data.twist.twist.linear.x
        self.vy_vesc_buffer = odom_data.twist.twist.linear.y

        # car orientation
        quaternion = (\
            odom_data.pose.pose.orientation.x, \
            odom_data.pose.pose.orientation.y, \
            odom_data.pose.pose.orientation.z, \
            odom_data.pose.pose.orientation.w \
            )
        euler = euler_from_quaternion(quaternion)
        self.yaw_vesc_buffer = euler[2]

        # car angular velocity
        self.yaw_rate_vesc_buffer = odom_data.twist.twist.angular.z
        # self.get_logger().info("Updating ODOM (vy): {self.vy}")

    def set_joy_command(self, joy_data):
        self.joy_speed_buffer = joy_data.drive.speed
        self.joy_steering_buffer = joy_data.drive.steering_angle

    def get_latest_measurements(self):
        # car speed
        self.vx = self.vx_vesc_buffer
        self.vy = self.vy_vesc_buffer

        # manual control
        self.joy_speed = self.joy_speed_buffer 
        self.joy_steering = self.joy_steering_buffer

        # state vector
        self.x[0] = self.x_pose_buffer
        self.x[1] = self.y_pose_buffer
        self.x[2] = self.yaw_pose_buffer
        self.x[3] = self.yaw_rate_imu_buffer
        self.x[4] = self.vy_vesc_buffer

    def cbf_controller(self):
        # This is the main function that computes the steering command depending on the barrier and the nominal control.

        #   Inputs
        #   ======
        #   x: float array of dimension 5 if dynamic bicyce model or 3 if unicycle model is used
        #       for bicycle model:
        #           X = x[0]  # (global X position)
        #           Y = x[1]  # (global Y position)
        #           vy = x[2]  # (lateral velocity)
        #           yaw = x[3]  # (global heading/yaw)
        #           yawd = x[4]  # (global heading/yaw rate)
        #
        #       for unicycle model:
        #           X = x[0]  # (global X position)
        #           Y = x[1]  # (global Y position)
        #           yaw = x[2]  # (global heading/yaw)
        #   unom: float (nominal steering input)
        #   car: object (see car_model_implementation.py)
        #   gains: dictionary of floats (CBF parameter)
        #   svm_params: dictionary of arrays of floats and floats (SVM model)
        #   alpha: float (CBF parameter)
        #   time_it: bool (if the computation of the control law should be timed or not
        #
        #   Outputs
        #   =======
        #   u - steering command to be implemented and recorded
        #   usafe - safe control (data to be recorded)
        #   unom - nominal control (data to be recorded)
        #   time_vec - computing time for  (data to be recorded)

        # Get latest measurement
        self.get_latest_measurements()

        # Get cbf outputs
        usafe, b2, b0, time_vec = self.getSafetyConstraintsBycicle()

        # Overwrite if nominal control is unsafe
        if b2 < 0:
            delta_raw = usafe
        else:
            delta_raw = self.joy_steering

        # Constant speed
        speed_raw = self.desired_speed

        # clamp values
        delta = self.clamp(delta_raw, self.max_right_steering, self.max_left_steering)
        speed = self.clamp(speed_raw, self.max_speed, self.min_speed)
        
        self.get_logger().info(f'\n'
                               f'\n delta:{delta_raw}'
                               f'\n speed:{speed_raw}'
                               f'\n clamped delta:{delta}'
                               f'\n clamped speed:{speed}'
                               )

        # Publish values
        try:
            # publish drive control signal
            self.drive_cmd.header.stamp = self.current_time
            self.drive_cmd.header.frame_id = self.frame_id
            self.drive_cmd.drive.speed = speed
            self.drive_cmd.drive.steering_angle = delta
            self.drive_pub.publish(self.drive_cmd)

        except KeyboardInterrupt:
            self.drive_cmd.header.stamp = self.current_time
            self.drive_cmd.header.frame_id = self.frame_id
            self.drive_cmd.drive.speed = self.zero_speed
            self.drive_cmd.drive.steering_angle = 0
            self.drive_pub.publish(self.drive_cmd)

    def clamp(self, value, upper_bound, lower_bound=None):
        if lower_bound==None:
            lower_bound = -upper_bound # making lower bound symmetric about zero
        if value < lower_bound:
            value_c = lower_bound
        elif value > upper_bound:
            value_c = upper_bound
        else:
            value_c = value
        return value_c 

    def getSafetyConstraintsUnicycle(self):
        xy = self.x[:2]
        h, g, H, time_vec = self.getBarrier(xy)
        v = float(np.sqrt(self.vx**2 + self.vy**2))

        xyd = v * np.array([np.cos(self.x[2]), np.sin(self.x[2])])
        xydd_x = v * np.array([-np.sin(self.x[2]), np.cos(self.x[2])])

        L = self.car.getParams()["L"]
        unom = self.joy_steering*np.pi/180  # rad2deg
        yaw_rate_nom = np.tan(unom*v/L)

        start_time = time.time()
        b0 = float(h)
        Lfb0 = xyd.T @ g
        b1 = Lfb0 + self.c1_gain * b0

        Lfb1 = xyd.T @ H @ xyd + self.c1_gain * Lfb0
        Lgb1 = xydd_x.T @ g
        b2 = float(Lfb1 + Lgb1 * yaw_rate_nom + self.c2_gain * b1)
        
        yaw_rate_safe = -(Lfb1 + self.c2_gain * b1) / Lgb1
        usafe = float(np.arctan(yaw_rate_safe * L/v))*180/np.pi
        end_time = time.time()
        if self.observe_computation_time:
            time_vec += end_time - start_time
        return usafe, b2, b0, time_vec

    def getSafetyConstraintsBycicle(self):

        # For input parameters see function above.
        # This function computes the safety control that might be implemented and additionally returns the CBF and its derivative

        # X = x[0]  # (global X position)
        # Y = x[1]  # (global Y position)
        vy = self.x[2]  # (lateral velocity)
        yaw = self.x[3]  # (global heading/yaw)
        yawd = self.x[4]  # (global heading/yaw rate)

        xy = self.x[:2]
        h, g, H, time_vec = self.getBarrier(xy)

        vx = self.vx
        unom = self.joy_steering
        sys_mat_a = self.car.getParams()["A"]
        b21 = self.car.getParams()["B"][1]

        xyd = np.array([vx * np.cos(yaw) - vy * np.sin(yaw), vx * np.sin(yaw) + vy * np.cos(yaw)])
        xydd_x = np.array([-(vx*np.sin(yaw)+vy*np.cos(yaw))*yawd-np.sin(yaw)*sys_mat_a[1,1:] @ x[2:],
                        (vx*np.cos(yaw)-vy*np.sin(yaw))*yawd+np.cos(yaw)*sys_mat_a[1,1:] @ x[2:]])
        xydd_u = np.array([-np.sin(yaw),np.cos(yaw)])*b21

        start_time = time.time()
        b0 = float(h)
        Lfb0 = xyd.T @ g
        b1 = Lfb0 + self.c1_gain * b0

        Lfb1 = xyd.T @ H @ xyd + xydd_x.T @ g + self.c1_gain * Lfb0
        Lgb1 = xydd_u.T @ g
        b2 = float(Lfb1 + Lgb1 * unom + self.c2_gain * b1)
        usafe = float(-(Lfb1 + self.c2_gain * b1) / Lgb1)
        end_time = time.time()
        if self.observe_computation_time:
            time_vec += end_time - start_time
        return usafe, b2, b0, time_vec

    def getBarrier(self, xy):

        # This function computes the barrier, its Jacobian and Hessian
        gamma = self.svm_params["gamma"]
        b = self.svm_params["b"]
        dc = self.svm_params["dc"]
        sv = self.svm_params["sv"]
        dim_sv = dc.shape[1]
        time_vec = 0

        start_time = time.time()
        Xi_X = sv - xy
        Xi_X_ip = Xi_X ** 2
        exponent = -gamma * np.sum(Xi_X_ip, axis=1)
        Hi = np.exp(exponent)

        # Barrier function
        hs = dc @ Hi
        h = hs + b - self.alpha

        # Gradient
        G = np.multiply(Xi_X.T, [Hi, Hi]) * dc
        g = 2 * gamma * np.sum(G, axis=1)

        # Hessian
        H = 2 * gamma * (2 * gamma * Xi_X.T * (Hi * dc) @ Xi_X - hs * np.eye(2))

        time_vec = (time.time() - start_time)
        if self.observe_computation_time:
            print("--- Vectorized computation: %ss." % time_vec)
        return h, g, H, time_vec


def main(args=None):
    rclpy.init(args=args)
    cbf_publisher = CbfSafetyFilter
()
    try:
        rclpy.spin(cbf_publisher)
        cbf_publisher.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        cbf_publisher.get_logger().info(f'Shutting down {NODE_NAME}...')
        cbf_publisher.drive_cmd.header.stamp = cbf_publisher.current_time
        cbf_publisher.drive_cmd.header.frame_id = cbf_publisher.frame_id
        cbf_publisher.drive_cmd.drive.speed = cbf_publisher.zero_speed
        cbf_publisher.drive_cmd.drive.steering_angle = 0.0
        cbf_publisher.drive_pub.publish(cbf_publisher.drive_cmd)
        time.sleep(1)
        cbf_publisher.destroy_node()
        rclpy.shutdown()
        cbf_publisher.get_logger().info(f'{NODE_NAME} shut down successfully.')


if __name__ == '__main__':
    main()
