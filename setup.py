from setuptools import setup

package_name = 'cbf'
cbf_submodule = str(package_name + "/cbf_submodule")
trained_models = str("/cbf_submodule")

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name, cbf_submodule],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.py')),
        (os.path.join('share', package_name, 'config'), glob('config/*.yaml')),
        (os.path.join('share', package_name, 'cbf_submodule'), glob('cbf_submodule/*.py')),
        (os.path.join('share', package_name, 'trained_models'),glob('trained_models/*.joblib'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='root',
    maintainer_email='djnighti@ucsd.edu',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'cbf_node = cbf.cbf_node:main',
        ],
    },
)
